/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package volvis;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;
import gui.RaycastRendererPanel;
import gui.TransferFunction2DEditor;
import gui.TransferFunctionEditor;
import java.awt.image.BufferedImage;
import util.TFChangeListener;
import util.VectorMath;
import volume.GradientVolume;
import volume.Volume;
import volume.VoxelGradient;

/**
 *
 * @author michel
 */
public class RaycastRenderer extends Renderer implements TFChangeListener {

    private Volume volume = null;
    private GradientVolume gradients = null;
    RaycastRendererPanel panel;
    TransferFunction tFunc;
    TransferFunctionEditor tfEditor;
    TransferFunction2DEditor tfEditor2D;
    public static int typeRendering;
    public static boolean isPhong;

    public final static int SLICER = 0;
    public final static int MIP = 1;
    public final static int COMPOSITING = 2;
    public final static int TRANSFER2D = 3;
    
    public static final int limit=100;

    public RaycastRenderer() {
        panel = new RaycastRendererPanel(this);
        panel.setSpeedLabel("0");
        isPhong = false;
        typeRendering = SLICER;
    }

    public void setVolume(Volume vol) {
        System.out.println("Assigning volume");
        volume = vol;

        System.out.println("Computing gradients");
        gradients = new GradientVolume(vol);

        // set up image for storing the resulting rendering
        // the image width and height are equal to the length of the volume diagonal
        int imageSize = (int) Math.floor(Math.sqrt(vol.getDimX() * vol.getDimX() + vol.getDimY() * vol.getDimY()
                + vol.getDimZ() * vol.getDimZ()));
        if (imageSize % 2 != 0) {
            imageSize = imageSize + 1;
        }
        image = new BufferedImage(imageSize, imageSize, BufferedImage.TYPE_INT_ARGB);
        // create a standard TF where lowest intensity maps to black, the highest to white, and opacity increases
        // linearly from 0.0 to 1.0 over the intensity range
        tFunc = new TransferFunction(volume.getMinimum(), volume.getMaximum());

        // uncomment this to initialize the TF with good starting values for the orange dataset 
        //tFunc.setTestFunc();
        tFunc.addTFChangeListener(this);
        tfEditor = new TransferFunctionEditor(tFunc, volume.getHistogram());

        tfEditor2D = new TransferFunction2DEditor(volume, gradients);
        tfEditor2D.addTFChangeListener(this);

        System.out.println("Finished initialization of RaycastRenderer");
    }

    public RaycastRendererPanel getPanel() {
        return panel;
    }

    public TransferFunction2DEditor getTF2DPanel() {
        return tfEditor2D;
    }

    public TransferFunctionEditor getTFPanel() {
        return tfEditor;
    }

    short getSafeVoxel(int xx, int yy, int zz) {
        if (xx <= 0 || yy <= 0 || zz <= 0 || xx >= volume.getDimX() || yy >= volume.getDimY() || zz >= volume.getDimZ()) {
            return 0;
        }
        return volume.getVoxel(xx, yy, zz);
    }

    short getVoxelByInterpolation(double[] coord) {

        if (coord[0] < 0 || coord[0] > volume.getDimX() || coord[1] < 0 || coord[1] > volume.getDimY()
                || coord[2] < 0 || coord[2] > volume.getDimZ()) {
            return 0;
        }

        int x = (int) Math.floor(coord[0]);
        int y = (int) Math.floor(coord[1]);
        int z = (int) Math.floor(coord[2]);

        int[] x0 = {x, y, z};
        int[] x1 = {x + 1, y, z};
        int[] x2 = {x, y + 1, z};
        int[] x3 = {x + 1, y + 1, z};
        int[] x4 = {x, y, z + 1};
        int[] x5 = {x + 1, y, z + 1};
        int[] x6 = {x, y + 1, z + 1};
        int[] x7 = {x + 1, y + 1, z + 1};

        double alfa = coord[0] - x;
        double beta = coord[1] - y;
        double gamma = coord[2] - z;

        return (short) ((1 - alfa) * (1 - beta) * (1 - gamma) * getSafeVoxel(x0[0], x0[1], x0[2])
                + alfa * (1 - beta) * (1 - gamma) * getSafeVoxel(x1[0], x1[1], x1[2])
                + (1 - alfa) * beta * (1 - gamma) * getSafeVoxel(x2[0], x2[1], x2[2])
                + alfa * beta * (1 - gamma) * getSafeVoxel(x3[0], x3[1], x3[2])
                + (1 - alfa) * (1 - beta) * gamma * getSafeVoxel(x4[0], x4[1], x4[2])
                + alfa * (1 - beta) * gamma * getSafeVoxel(x5[0], x5[1], x5[2])
                + (1 - alfa) * beta * gamma * getSafeVoxel(x6[0], x6[1], x6[2])
                + alfa * beta * gamma * getSafeVoxel(x7[0], x7[1], x7[2]));
        //return volume.getVoxel(x, y, z);
    }

    short getVoxel(double[] coord) {

        if (coord[0] < 0 || coord[0] > volume.getDimX() || coord[1] < 0 || coord[1] > volume.getDimY()
                || coord[2] < 0 || coord[2] > volume.getDimZ()) {
            return 0;
        }

        int x = (int) Math.floor(coord[0]);
        int y = (int) Math.floor(coord[1]);
        int z = (int) Math.floor(coord[2]);

        return getSafeVoxel(x, y, z);
    }

    void slicerPhong(double[] viewMatrix) {

        // clear image
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                image.setRGB(i, j, 0);
            }
        }

        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        double[] viewVec = new double[3];
        double[] uVec = new double[3];
        double[] vVec = new double[3];
        VectorMath.setVector(viewVec, viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        VectorMath.setVector(uVec, viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        VectorMath.setVector(vVec, viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        // image is square
        int imageCenter = image.getWidth() / 2;

        double[] pixelCoord = new double[3];
        double[] volumeCenter = new double[3];
        VectorMath.setVector(volumeCenter, volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);

        // sample on a plane through the origin of the volume data
        double max = volume.getMaximum();
        TFColor voxelColor = new TFColor();

        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                pixelCoord[0] = uVec[0] * (i - imageCenter) + vVec[0] * (j - imageCenter)
                        + volumeCenter[0];//+ viewVec[0]*(volumeCenter[0]);
                pixelCoord[1] = uVec[1] * (i - imageCenter) + vVec[1] * (j - imageCenter)
                        + volumeCenter[1];//+ viewVec[1]*(volumeCenter[1]);
                pixelCoord[2] = uVec[2] * (i - imageCenter) + vVec[2] * (j - imageCenter)
                        + volumeCenter[2];//+ viewVec[2]*(volumeCenter[2]);

                short intensity = getVoxelByInterpolation(pixelCoord);

                //Assumption: L==V
                double V[] = {pixelCoord[0] / VectorMath.length(pixelCoord), pixelCoord[1] / VectorMath.length(pixelCoord), pixelCoord[2] / VectorMath.length(pixelCoord)};
                double TwoV[] = {2 * V[0], 2 * V[1], 2 * V[2]};
                double H[] = {TwoV[0] / VectorMath.length(TwoV), TwoV[1] / VectorMath.length(TwoV), TwoV[2] / VectorMath.length(TwoV)};

                VoxelGradient gr = gradients.getGradient(pixelCoord[0], pixelCoord[1], pixelCoord[2]);
                double N[] = {0, 0, 0};
                //compute normal vector
                if (gr.mag != 0) {
                    N[0] = gr.x / gr.mag;
                    N[1] = gr.y / gr.mag;
                    N[2] = gr.z / gr.mag;
                }
                double ci = 0;
                if (VectorMath.dotproduct(V, N) > 0 && VectorMath.dotproduct(N, H) > 0) {
                    ci = 0.1 + intensity * 0.7 * VectorMath.dotproduct(V, N) + 0.2 * Math.pow(VectorMath.dotproduct(N, H), 10);
                }
                int val = (int) ci;

                // Map the intensity to a grey value by linear scaling
                voxelColor.r = val / max;
                voxelColor.g = voxelColor.r;
                voxelColor.b = voxelColor.r;
                voxelColor.a = val > 0 ? 1.0 : 0.0;
                // this makes intensity 0 completely transparent and the rest opaque
                // Alternatively, apply the transfer function to obtain a color
                //voxelColor = tFunc.getColor(val);

                // BufferedImage expects a pixel color packed as ARGB in an int
                int c_alpha = voxelColor.a <= 1.0 ? (int) Math.floor(voxelColor.a * 255) : 255;
                int c_red = voxelColor.r <= 1.0 ? (int) Math.floor(voxelColor.r * 255) : 255;
                int c_green = voxelColor.g <= 1.0 ? (int) Math.floor(voxelColor.g * 255) : 255;
                int c_blue = voxelColor.b <= 1.0 ? (int) Math.floor(voxelColor.b * 255) : 255;
                int pixelColor = (c_alpha << 24) | (c_red << 16) | (c_green << 8) | c_blue;
                image.setRGB(i, j, pixelColor);
            }
        }
    }

    void MIPPhong(double[] viewMatrix) {
        // clear image
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                image.setRGB(i, j, 0);
            }
        }

        double[] viewVec = new double[3];
        double[] uVec = new double[3];
        double[] vVec = new double[3];

        VectorMath.setVector(viewVec, viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        VectorMath.setVector(uVec, viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        VectorMath.setVector(vVec, viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        System.out.println("viewVec=" + viewVec[0] + "," + viewVec[1] + "," + viewVec[2]);
        System.out.println("uVec=" + uVec[0] + "," + uVec[1] + "," + uVec[2]);
        System.out.println("vVec=" + vVec[0] + "," + vVec[1] + "," + vVec[2]);

        System.out.println("view x u=" + (viewVec[0] * uVec[0] + viewVec[1] * uVec[1] + viewVec[2] * uVec[2]));
        System.out.println("view x v=" + (viewVec[0] * vVec[0] + viewVec[1] * vVec[1] + viewVec[2] * vVec[2]));
        System.out.println("u x v=" + (vVec[0] * uVec[0] + vVec[1] * uVec[1] + vVec[2] * uVec[2]));

        // image is square
        int imageCenter = image.getWidth() / 2;

        double[] pixelCoord = new double[3];
        double[] volumeCenter = new double[3];
        double[] volumeCenterUnit = new double[3];

        VectorMath.setVector(volumeCenter, volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);

        // sample on a plane through the origin of the volume data
        double max = volume.getMaximum();
        TFColor voxelColor = new TFColor();

        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                pixelCoord[0] = uVec[0] * (i - imageCenter) + vVec[0] * (j - imageCenter)
                        + volumeCenter[0];
                pixelCoord[1] = uVec[1] * (i - imageCenter) + vVec[1] * (j - imageCenter)
                        + volumeCenter[1];
                pixelCoord[2] = uVec[2] * (i - imageCenter) + vVec[2] * (j - imageCenter)
                        + volumeCenter[2];
                double px = pixelCoord[0];
                double py = pixelCoord[1];
                double pz = pixelCoord[2];

                int val = 0;
                for (int kk = limit; kk >= -limit; kk--) {
                    pixelCoord[0] = px
                            + viewVec[0] * ((kk / (double) limit) * volumeCenter[0]);
                    pixelCoord[1] = py
                            + viewVec[1] * ((kk / (double) limit) * volumeCenter[1]);
                    pixelCoord[2] = pz
                            + viewVec[2] * ((kk / (double) limit) * volumeCenter[2]);
                    short intensity = getVoxelByInterpolation(pixelCoord);

                    //Assumption: L==V
                    double V[] = {pixelCoord[0] / VectorMath.length(pixelCoord), pixelCoord[1] / VectorMath.length(pixelCoord), pixelCoord[2] / VectorMath.length(pixelCoord)};
                    double TwoV[] = {2 * V[0], 2 * V[1], 2 * V[2]};
                    double H[] = {TwoV[0] / VectorMath.length(TwoV), TwoV[1] / VectorMath.length(TwoV), TwoV[2] / VectorMath.length(TwoV)};

                    VoxelGradient gr = gradients.getGradient(pixelCoord[0], pixelCoord[1], pixelCoord[2]);
                    double N[] = {0, 0, 0};
                    //compute normal vector
                    if (gr.mag != 0) {
                        N[0] = gr.x / gr.mag;
                        N[1] = gr.y / gr.mag;
                        N[2] = gr.z / gr.mag;
                    }
                    double ci = 0;
                    if (VectorMath.dotproduct(V, N) > 0 && VectorMath.dotproduct(N, H) > 0) {
                        ci = 0.1 + intensity * 0.7 * VectorMath.dotproduct(V, N) + 0.2 * Math.pow(VectorMath.dotproduct(N, H), 10);
                    }

                    val = (int) Math.max(val, ci);
                }

                // Map the intensity to a grey value by linear scaling
                voxelColor.r = val / max;
                voxelColor.g = voxelColor.r;
                voxelColor.b = voxelColor.r;
                voxelColor.a = val > 0 ? 1.0 : 0.0;
                // this makes intensity 0 completely transparent and the rest opaque
                // Alternatively, apply the transfer function to obtain a color
                //voxelColor = tFunc.getColor(val);

                // BufferedImage expects a pixel color packed as ARGB in an int
                int c_alpha = voxelColor.a <= 1.0 ? (int) Math.floor(voxelColor.a * 255) : 255;
                int c_red = voxelColor.r <= 1.0 ? (int) Math.floor(voxelColor.r * 255) : 255;
                int c_green = voxelColor.g <= 1.0 ? (int) Math.floor(voxelColor.g * 255) : 255;
                int c_blue = voxelColor.b <= 1.0 ? (int) Math.floor(voxelColor.b * 255) : 255;
                int pixelColor = (c_alpha << 24) | (c_red << 16) | (c_green << 8) | c_blue;
                image.setRGB(i, j, pixelColor);
            }
        }
    }

    void compositingPhong(double[] viewMatrix) {
        // clear image
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                image.setRGB(i, j, 0);
            }
        }

        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        double[] viewVec = new double[3];
        double[] uVec = new double[3];
        double[] vVec = new double[3];

        VectorMath.setVector(viewVec, viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        VectorMath.setVector(uVec, viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        VectorMath.setVector(vVec, viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        System.out.println("viewVec=" + viewVec[0] + "," + viewVec[1] + "," + viewVec[2]);
        System.out.println("uVec=" + uVec[0] + "," + uVec[1] + "," + uVec[2]);
        System.out.println("vVec=" + vVec[0] + "," + vVec[1] + "," + vVec[2]);

        System.out.println("view x u=" + (viewVec[0] * uVec[0] + viewVec[1] * uVec[1] + viewVec[2] * uVec[2]));
        System.out.println("view x v=" + (viewVec[0] * vVec[0] + viewVec[1] * vVec[1] + viewVec[2] * vVec[2]));
        System.out.println("u x v=" + (vVec[0] * uVec[0] + vVec[1] * uVec[1] + vVec[2] * uVec[2]));

        // image is square
        int imageCenter = image.getWidth() / 2;

        double[] pixelCoord = new double[3];
        double[] volumeCenter = new double[3];

        VectorMath.setVector(volumeCenter, volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);

        // sample on a plane through the origin of the volume data
        double max = volume.getMaximum();
        TFColor voxelColor = new TFColor();

        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                pixelCoord[0] = uVec[0] * (i - imageCenter) + vVec[0] * (j - imageCenter)
                        + volumeCenter[0];
                pixelCoord[1] = uVec[1] * (i - imageCenter) + vVec[1] * (j - imageCenter)
                        + volumeCenter[1];
                pixelCoord[2] = uVec[2] * (i - imageCenter) + vVec[2] * (j - imageCenter)
                        + volumeCenter[2];
                double px = pixelCoord[0];
                double py = pixelCoord[1];
                double pz = pixelCoord[2];

                int val = 0;
                double c[] = new double[202];
                double rr[] = new double[202];
                double gg[] = new double[202];
                double bb[] = new double[202];
                double alfatot = 1;

                for (int kk = limit; kk >= -limit; kk--) {
                    pixelCoord[0] = px + viewVec[0] * ((kk / (double) limit) * volumeCenter[0]);
                    pixelCoord[1] = py
                            + viewVec[1] * ((kk / (double) limit) * volumeCenter[1]);
                    pixelCoord[2] = pz
                            + viewVec[2] * ((kk / (double) limit) * volumeCenter[2]);
                    TFColor colorData = tFunc.getColor(getVoxelByInterpolation(pixelCoord));
                    alfatot *= (1 - colorData.a);

                    //Assumption: L==V
                    double V[] = {pixelCoord[0] / VectorMath.length(pixelCoord), pixelCoord[1] / VectorMath.length(pixelCoord), pixelCoord[2] / VectorMath.length(pixelCoord)};
                    double TwoV[] = {2 * V[0], 2 * V[1], 2 * V[2]};
                    double H[] = {TwoV[0] / VectorMath.length(TwoV), TwoV[1] / VectorMath.length(TwoV), TwoV[2] / VectorMath.length(TwoV)};

                    VoxelGradient gr = gradients.getGradient(pixelCoord[0], pixelCoord[1], pixelCoord[2]);
                    double N[] = {0, 0, 0};
                    //compute normal vector
                    if (gr.mag != 0) {
                        N[0] = gr.x / gr.mag;
                        N[1] = gr.y / gr.mag;
                        N[2] = gr.z / gr.mag;
                    }
                    double cr = 0;
                    double cg = 0;
                    double cb = 0;
                    if (VectorMath.dotproduct(V, N) > 0 && VectorMath.dotproduct(N, H) > 0) {
                        cr = 0.1 + colorData.r * 0.7 * VectorMath.dotproduct(V, N) + 0.2 * Math.pow(VectorMath.dotproduct(N, H), 10);
                        cg = 0.1 + colorData.g * 0.7 * VectorMath.dotproduct(V, N) + 0.2 * Math.pow(VectorMath.dotproduct(N, H), 10);
                        cb = 0.1 + colorData.b * 0.7 * VectorMath.dotproduct(V, N) + 0.2 * Math.pow(VectorMath.dotproduct(N, H), 10);
                    }
                    rr[-kk + limit + 1] = (colorData.a * cr) + (1 - colorData.a) * (rr[-kk + limit]);
                    gg[-kk + limit + 1] = (colorData.a * cg) + (1 - colorData.a) * (gg[-kk + limit]);
                    bb[-kk + limit + 1] = (colorData.a * cb) + (1 - colorData.a) * (bb[-kk + limit]);
                }
                voxelColor.r = rr[2 * limit + 1];
                voxelColor.g = gg[2 * limit + 1];
                voxelColor.b = bb[2 * limit + 1];
                voxelColor.a = 1 - alfatot;

                int c_alpha = voxelColor.a <= 1.0 ? (int) Math.floor(voxelColor.a * 255) : 255;
                int c_red = voxelColor.r <= 1.0 ? (int) Math.floor(voxelColor.r * 255) : 255;
                int c_green = voxelColor.g <= 1.0 ? (int) Math.floor(voxelColor.g * 255) : 255;
                int c_blue = voxelColor.b <= 1.0 ? (int) Math.floor(voxelColor.b * 255) : 255;
                int pixelColor = (c_alpha << 24) | (c_red << 16) | (c_green << 8) | c_blue;
                image.setRGB(i, j, pixelColor);
            }
        }
    }

    void transferFunction2DPhong(double[] viewMatrix) {
        // clear image
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                image.setRGB(i, j, 0);
            }
        }
        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        double[] viewVec = new double[3];
        double[] uVec = new double[3];
        double[] vVec = new double[3];

        VectorMath.setVector(viewVec, viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        VectorMath.setVector(uVec, viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        VectorMath.setVector(vVec, viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        System.out.println("viewVec=" + viewVec[0] + "," + viewVec[1] + "," + viewVec[2]);
        System.out.println("uVec=" + uVec[0] + "," + uVec[1] + "," + uVec[2]);
        System.out.println("vVec=" + vVec[0] + "," + vVec[1] + "," + vVec[2]);

        System.out.println("view x u=" + (viewVec[0] * uVec[0] + viewVec[1] * uVec[1] + viewVec[2] * uVec[2]));
        System.out.println("view x v=" + (viewVec[0] * vVec[0] + viewVec[1] * vVec[1] + viewVec[2] * vVec[2]));
        System.out.println("u x v=" + (vVec[0] * uVec[0] + vVec[1] * uVec[1] + vVec[2] * uVec[2]));

        // image is square
        int imageCenter = image.getWidth() / 2;

        double[] pixelCoord = new double[3];
        double[] volumeCenter = new double[3];

        VectorMath.setVector(volumeCenter, volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);

        // sample on a plane through the origin of the volume data
        double max = volume.getMaximum();
        TFColor voxelColor = new TFColor();
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                pixelCoord[0] = uVec[0] * (i - imageCenter) + vVec[0] * (j - imageCenter)
                        + volumeCenter[0];
                pixelCoord[1] = uVec[1] * (i - imageCenter) + vVec[1] * (j - imageCenter)
                        + volumeCenter[1];
                pixelCoord[2] = uVec[2] * (i - imageCenter) + vVec[2] * (j - imageCenter)
                        + volumeCenter[2];
                double px = pixelCoord[0];
                double py = pixelCoord[1];
                double pz = pixelCoord[2];

                int val = 0;
                double rr[] = new double[2 * limit + 2];
                double gg[] = new double[2 * limit + 2];
                double bb[] = new double[2 * limit + 2];
                double alfaLayer[] = new double[2 * limit + 2];
                double N[] = {0.0, 0.0, 0.0};

                for (int kk = limit; kk >= -limit; kk--) {
                    pixelCoord[0] = px
                            + viewVec[0] * ((kk / (double) limit) * volumeCenter[0]);
                    pixelCoord[1] = py
                            + viewVec[1] * ((kk / (double) limit) * volumeCenter[1]);
                    pixelCoord[2] = pz
                            + viewVec[2] * ((kk / (double) limit) * volumeCenter[2]);

                    //Assumption: L==V
                    double V[] = {pixelCoord[0] / VectorMath.length(pixelCoord), pixelCoord[1] / VectorMath.length(pixelCoord), pixelCoord[2] / VectorMath.length(pixelCoord)};
                    double TwoV[] = {2 * V[0], 2 * V[1], 2 * V[2]};
                    double H[] = {TwoV[0] / VectorMath.length(TwoV), TwoV[1] / VectorMath.length(TwoV), TwoV[2] / VectorMath.length(TwoV)};

                    short fv = this.tfEditor2D.triangleWidget.baseIntensity;
                    double alfav = this.tfEditor2D.triangleWidget.color.a;
                    double alfax;

                    short fx = getVoxelByInterpolation(pixelCoord);
                    double r = this.tfEditor2D.triangleWidget.radius;

                    VoxelGradient gr = gradients.getGradient(pixelCoord[0], pixelCoord[1], pixelCoord[2]);

                    if (gr.mag == 0 && fx == fv) {
                        alfax = alfav;
                    } else if (gr.mag > 0 && fx - r * gr.mag <= fv && fv <= fx + r * gr.mag) {
                        alfax = alfav * (1 - (1 / r) * (Math.abs(fv - fx) / gr.mag));
                    } else {
                        alfax = 0;
                    }
                    alfaLayer[kk + limit] = alfax;

                    //compute normal vector
                    if (gr.mag != 0) {
                        N[0] = gr.x / gr.mag;
                        N[1] = gr.y / gr.mag;
                        N[2] = gr.z / gr.mag;
                    } else {
                        N[0] = 0;
                        N[1] = 0;
                        N[2] = 0;
                    }
                    double cr = 0;
                    double cg = 0;
                    double cb = 0;
                    if (VectorMath.dotproduct(V, N) > 0 && VectorMath.dotproduct(N, H) > 0) {
                        cr = 0.1 + tfEditor2D.triangleWidget.color.r * 0.7 * VectorMath.dotproduct(V, N) + 0.2 * Math.pow(VectorMath.dotproduct(N, H), 10);
                        cg = 0.1 + tfEditor2D.triangleWidget.color.g * 0.7 * VectorMath.dotproduct(V, N) + 0.2 * Math.pow(VectorMath.dotproduct(N, H), 10);
                        cb = 0.1 + tfEditor2D.triangleWidget.color.b * 0.7 * VectorMath.dotproduct(V, N) + 0.2 * Math.pow(VectorMath.dotproduct(N, H), 10);
                    }
                    rr[-kk + limit + 1] = (alfax * cr) + (1 - alfax) * (rr[-kk + limit]);
                    gg[-kk + limit + 1] = (alfax * cg) + (1 - alfax) * (gg[-kk + limit]);
                    bb[-kk + limit + 1] = (alfax * cb) + (1 - alfax) * (bb[-kk + limit]);
                }

                double alfatot = 1;
                for (int kk = 0; kk <= 2 * limit + 1; kk++) {
                    alfatot = alfatot * (1 - alfaLayer[kk]);
                }

                voxelColor.r = rr[2 * limit + 1];
                voxelColor.g = gg[2 * limit + 1];
                voxelColor.b = bb[2 * limit + 1];
                voxelColor.a = 1 - alfatot;

                // BufferedImage expects a pixel color packed as ARGB in an int
                int c_alpha = voxelColor.a <= 1.0 ? (int) Math.floor(voxelColor.a * 255) : 255;
                int c_red = voxelColor.r <= 1.0 ? (int) Math.floor(voxelColor.r * 255) : 255;
                int c_green = voxelColor.g <= 1.0 ? (int) Math.floor(voxelColor.g * 255) : 255;
                int c_blue = voxelColor.b <= 1.0 ? (int) Math.floor(voxelColor.b * 255) : 255;
                int pixelColor = (c_alpha << 24) | (c_red << 16) | (c_green << 8) | c_blue;
                image.setRGB(i, j, pixelColor);
            }
        }
    }

    void transferFunction2D(double[] viewMatrix) {

        // clear image
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                image.setRGB(i, j, 0);
            }
        }

        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        double[] viewVec = new double[3];
        double[] uVec = new double[3];
        double[] vVec = new double[3];

        VectorMath.setVector(viewVec, viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        VectorMath.setVector(uVec, viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        VectorMath.setVector(vVec, viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        System.out.println("viewVec=" + viewVec[0] + "," + viewVec[1] + "," + viewVec[2]);
        System.out.println("uVec=" + uVec[0] + "," + uVec[1] + "," + uVec[2]);
        System.out.println("vVec=" + vVec[0] + "," + vVec[1] + "," + vVec[2]);

        System.out.println("view x u=" + (viewVec[0] * uVec[0] + viewVec[1] * uVec[1] + viewVec[2] * uVec[2]));
        System.out.println("view x v=" + (viewVec[0] * vVec[0] + viewVec[1] * vVec[1] + viewVec[2] * vVec[2]));
        System.out.println("u x v=" + (vVec[0] * uVec[0] + vVec[1] * uVec[1] + vVec[2] * uVec[2]));

        // image is square
        int imageCenter = image.getWidth() / 2;

        double[] pixelCoord = new double[3];
        double[] volumeCenter = new double[3];

        VectorMath.setVector(volumeCenter, volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);

        // sample on a plane through the origin of the volume data
        double max = volume.getMaximum();
        TFColor voxelColor = new TFColor();
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                pixelCoord[0] = uVec[0] * (i - imageCenter) + vVec[0] * (j - imageCenter)
                        + volumeCenter[0];
                pixelCoord[1] = uVec[1] * (i - imageCenter) + vVec[1] * (j - imageCenter)
                        + volumeCenter[1];
                pixelCoord[2] = uVec[2] * (i - imageCenter) + vVec[2] * (j - imageCenter)
                        + volumeCenter[2];
                double px = pixelCoord[0];
                double py = pixelCoord[1];
                double pz = pixelCoord[2];

                int val = 0;
                double alfaLayer[] = new double[2 * limit + 2];
                double c[] = new double[2 * limit + 2];

                for (int kk = limit; kk >= -limit; kk--) {
                    pixelCoord[0] = px
                            + viewVec[0] * ((kk / (double) limit) * volumeCenter[0]);
                    pixelCoord[1] = py
                            + viewVec[1] * ((kk / (double) limit) * volumeCenter[1]);
                    pixelCoord[2] = pz
                            + viewVec[2] * ((kk / (double) limit) * volumeCenter[2]);

                    short fv = this.tfEditor2D.triangleWidget.baseIntensity;
                    double alfav = this.tfEditor2D.triangleWidget.color.a;
                    double alfax;

                    short fx = getVoxelByInterpolation(pixelCoord);
                    double r = this.tfEditor2D.triangleWidget.radius;

                    VoxelGradient gr = gradients.getGradient(pixelCoord[0], pixelCoord[1], pixelCoord[2]);
                    if (gr.mag < tfEditor2D.triangleWidget.minKniss || gr.mag > tfEditor2D.triangleWidget.maxKniss) {
                        alfax = 0;
                    } else if (gr.mag == 0 && fx == fv) {
                        alfax = alfav;
                    } else if (gr.mag > 0 && fx - r * gr.mag <= fv && fv <= fx + r * gr.mag) {
                        alfax = alfav * (1 - (1 / r) * (Math.abs(fv - fx) / gr.mag));
                    } else {
                        alfax = 0;
                    }
                    alfaLayer[-kk + limit] = alfax;
                    //val=(int) Math.max(val, fx);
                    //c[kk+101]=alfax*getVoxelByInterpolation(pixelCoord)+(1-alfax)*c[kk+101-1];
                }
                double alfatot = 1;
                for (int kk = 0; kk <= 2 * limit + 1; kk++) {
                    alfatot = alfatot * (1 - alfaLayer[kk]);
                }
                //c[kk]=colorData.a*getVoxelByInterpolation(pixelCoord)+(1-colorData.a)*c[kk-1];

                voxelColor.r = tfEditor2D.triangleWidget.color.r;
                voxelColor.g = tfEditor2D.triangleWidget.color.g;
                voxelColor.b = tfEditor2D.triangleWidget.color.b;
                voxelColor.a = 1 - alfatot;

                // Alternatively, apply the transfer function to obtain a color
                // voxelColor = tfEditor2D.triangleWidget.color;//.getColor(val);
                //  System.out.println(voxelColor.r+" "+voxelColor.g+" "+voxelColor.b+" "+voxelColor.a);
                //voxelColor.a = 1-alfatot;
                // BufferedImage expects a pixel color packed as ARGB in an int
                int c_alpha = voxelColor.a <= 1.0 ? (int) Math.floor(voxelColor.a * 255) : 255;
                int c_red = voxelColor.r <= 1.0 ? (int) Math.floor(voxelColor.r * 255) : 255;
                int c_green = voxelColor.g <= 1.0 ? (int) Math.floor(voxelColor.g * 255) : 255;
                int c_blue = voxelColor.b <= 1.0 ? (int) Math.floor(voxelColor.b * 255) : 255;
                int pixelColor = (c_alpha << 24) | (c_red << 16) | (c_green << 8) | c_blue;
                image.setRGB(i, j, pixelColor);
            }
        }
    }

    void compositing(double[] viewMatrix) {
        // clear image
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                image.setRGB(i, j, 0);
            }
        }

        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        double[] viewVec = new double[3];
        double[] uVec = new double[3];
        double[] vVec = new double[3];

        VectorMath.setVector(viewVec, viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        VectorMath.setVector(uVec, viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        VectorMath.setVector(vVec, viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        System.out.println("viewVec=" + viewVec[0] + "," + viewVec[1] + "," + viewVec[2]);
        System.out.println("uVec=" + uVec[0] + "," + uVec[1] + "," + uVec[2]);
        System.out.println("vVec=" + vVec[0] + "," + vVec[1] + "," + vVec[2]);

        System.out.println("view x u=" + (viewVec[0] * uVec[0] + viewVec[1] * uVec[1] + viewVec[2] * uVec[2]));
        System.out.println("view x v=" + (viewVec[0] * vVec[0] + viewVec[1] * vVec[1] + viewVec[2] * vVec[2]));
        System.out.println("u x v=" + (vVec[0] * uVec[0] + vVec[1] * uVec[1] + vVec[2] * uVec[2]));

        // image is square
        int imageCenter = image.getWidth() / 2;

        double[] pixelCoord = new double[3];
        double[] volumeCenter = new double[3];
        double[] volumeCenterUnit = new double[3];

        VectorMath.setVector(volumeCenter, volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);
        double volumeCenterLength = VectorMath.length(volumeCenter);
        VectorMath.setVector(volumeCenterUnit, volume.getDimX() / (volumeCenterLength), volume.getDimY() / (volumeCenterLength), volume.getDimZ() / (volumeCenterLength));

        // sample on a plane through the origin of the volume data
        double max = volume.getMaximum();
        TFColor voxelColor = new TFColor();

        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                pixelCoord[0] = uVec[0] * (i - imageCenter) + vVec[0] * (j - imageCenter)
                        + volumeCenter[0];
                pixelCoord[1] = uVec[1] * (i - imageCenter) + vVec[1] * (j - imageCenter)
                        + volumeCenter[1];
                pixelCoord[2] = uVec[2] * (i - imageCenter) + vVec[2] * (j - imageCenter)
                        + volumeCenter[2];
                double px = pixelCoord[0];
                double py = pixelCoord[1];
                double pz = pixelCoord[2];

                int val = 0;
                double c[] = new double[202];
                double rr[] = new double[202];
                double gg[] = new double[202];
                double bb[] = new double[202];
                double alfatot = 1;

                for (int kk = limit; kk >= -limit; kk--) {
                    pixelCoord[0] = px + viewVec[0] * ((kk / (double) limit) * volumeCenter[0]);
                    pixelCoord[1] = py
                            + viewVec[1] * ((kk / (double) limit) * volumeCenter[1]);
                    pixelCoord[2] = pz
                            + viewVec[2] * ((kk / (double) limit) * volumeCenter[2]);
                    TFColor colorData = tFunc.getColor(getVoxelByInterpolation(pixelCoord));
                    rr[-kk + limit + 1] = colorData.a * colorData.r + (1 - colorData.a) * rr[-kk + limit];
                    gg[-kk + limit + 1] = colorData.a * colorData.g + (1 - colorData.a) * gg[-kk + limit];
                    bb[-kk + limit + 1] = colorData.a * colorData.b + (1 - colorData.a) * bb[-kk + limit];
                    alfatot *= (1 - colorData.a);
                }
                voxelColor.r = rr[2 * limit + 1];
                voxelColor.g = gg[2 * limit + 1];
                voxelColor.b = bb[2 * limit + 1];
                voxelColor.a = 1 - alfatot;

                int c_alpha = voxelColor.a <= 1.0 ? (int) Math.floor(voxelColor.a * 255) : 255;
                int c_red = voxelColor.r <= 1.0 ? (int) Math.floor(voxelColor.r * 255) : 255;
                int c_green = voxelColor.g <= 1.0 ? (int) Math.floor(voxelColor.g * 255) : 255;
                int c_blue = voxelColor.b <= 1.0 ? (int) Math.floor(voxelColor.b * 255) : 255;
                int pixelColor = (c_alpha << 24) | (c_red << 16) | (c_green << 8) | c_blue;
                image.setRGB(i, j, pixelColor);
            }
        }
    }

    void MIP(double[] viewMatrix) {

        // clear image
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                image.setRGB(i, j, 0);
            }
        }

        double[] viewVec = new double[3];
        double[] uVec = new double[3];
        double[] vVec = new double[3];

        VectorMath.setVector(viewVec, viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        VectorMath.setVector(uVec, viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        VectorMath.setVector(vVec, viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        System.out.println("viewVec=" + viewVec[0] + "," + viewVec[1] + "," + viewVec[2]);
        System.out.println("uVec=" + uVec[0] + "," + uVec[1] + "," + uVec[2]);
        System.out.println("vVec=" + vVec[0] + "," + vVec[1] + "," + vVec[2]);

        System.out.println("view x u=" + (viewVec[0] * uVec[0] + viewVec[1] * uVec[1] + viewVec[2] * uVec[2]));
        System.out.println("view x v=" + (viewVec[0] * vVec[0] + viewVec[1] * vVec[1] + viewVec[2] * vVec[2]));
        System.out.println("u x v=" + (vVec[0] * uVec[0] + vVec[1] * uVec[1] + vVec[2] * uVec[2]));

        // image is square
        int imageCenter = image.getWidth() / 2;

        double[] pixelCoord = new double[3];
        double[] volumeCenter = new double[3];
        double[] volumeCenterUnit = new double[3];

        VectorMath.setVector(volumeCenter, volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);
        double volumeCenterLength = VectorMath.length(volumeCenter);
        VectorMath.setVector(volumeCenterUnit, volume.getDimX() / (volumeCenterLength), volume.getDimY() / (volumeCenterLength), volume.getDimZ() / (volumeCenterLength));

        // sample on a plane through the origin of the volume data
        double max = volume.getMaximum();
        TFColor voxelColor = new TFColor();

        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                pixelCoord[0] = uVec[0] * (i - imageCenter) + vVec[0] * (j - imageCenter)
                        + volumeCenter[0];
                pixelCoord[1] = uVec[1] * (i - imageCenter) + vVec[1] * (j - imageCenter)
                        + volumeCenter[1];
                pixelCoord[2] = uVec[2] * (i - imageCenter) + vVec[2] * (j - imageCenter)
                        + volumeCenter[2];
                double px = pixelCoord[0];
                double py = pixelCoord[1];
                double pz = pixelCoord[2];

                int val = 0;
                for (int kk = limit; kk >= -limit; kk--) {
                    pixelCoord[0] = px
                            + viewVec[0] * ((kk / (double) limit) * volumeCenter[0]);
                    pixelCoord[1] = py
                            + viewVec[1] * ((kk / (double) limit) * volumeCenter[1]);
                    pixelCoord[2] = pz
                            + viewVec[2] * ((kk / (double) limit) * volumeCenter[2]);
                    val = Math.max(val, getVoxelByInterpolation(pixelCoord));
                }

                // Map the intensity to a grey value by linear scaling
                voxelColor.r = val / max;
                voxelColor.g = voxelColor.r;
                voxelColor.b = voxelColor.r;
                voxelColor.a = val > 0 ? 1.0 : 0.0;
                // this makes intensity 0 completely transparent and the rest opaque
                // Alternatively, apply the transfer function to obtain a color
                //voxelColor = tFunc.getColor(val);

                // BufferedImage expects a pixel color packed as ARGB in an int
                int c_alpha = voxelColor.a <= 1.0 ? (int) Math.floor(voxelColor.a * 255) : 255;
                int c_red = voxelColor.r <= 1.0 ? (int) Math.floor(voxelColor.r * 255) : 255;
                int c_green = voxelColor.g <= 1.0 ? (int) Math.floor(voxelColor.g * 255) : 255;
                int c_blue = voxelColor.b <= 1.0 ? (int) Math.floor(voxelColor.b * 255) : 255;
                int pixelColor = (c_alpha << 24) | (c_red << 16) | (c_green << 8) | c_blue;
                image.setRGB(i, j, pixelColor);
            }
        }
    }

    void slicer(double[] viewMatrix) {

        // clear image
        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                image.setRGB(i, j, 0);
            }
        }

        // vector uVec and vVec define a plane through the origin, 
        // perpendicular to the view vector viewVec
        double[] viewVec = new double[3];
        double[] uVec = new double[3];
        double[] vVec = new double[3];
        VectorMath.setVector(viewVec, viewMatrix[2], viewMatrix[6], viewMatrix[10]);
        VectorMath.setVector(uVec, viewMatrix[0], viewMatrix[4], viewMatrix[8]);
        VectorMath.setVector(vVec, viewMatrix[1], viewMatrix[5], viewMatrix[9]);

        // image is square
        int imageCenter = image.getWidth() / 2;

        double[] pixelCoord = new double[3];
        double[] volumeCenter = new double[3];
        VectorMath.setVector(volumeCenter, volume.getDimX() / 2, volume.getDimY() / 2, volume.getDimZ() / 2);

        // sample on a plane through the origin of the volume data
        double max = volume.getMaximum();
        TFColor voxelColor = new TFColor();

        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {
                pixelCoord[0] = uVec[0] * (i - imageCenter) + vVec[0] * (j - imageCenter)
                        + volumeCenter[0];//+ viewVec[0]*(volumeCenter[0]);
                pixelCoord[1] = uVec[1] * (i - imageCenter) + vVec[1] * (j - imageCenter)
                        + volumeCenter[1];//+ viewVec[1]*(volumeCenter[1]);
                pixelCoord[2] = uVec[2] * (i - imageCenter) + vVec[2] * (j - imageCenter)
                        + volumeCenter[2];//+ viewVec[2]*(volumeCenter[2]);

                int val = getVoxel(pixelCoord);

                // Map the intensity to a grey value by linear scaling
                voxelColor.r = val / max;
                voxelColor.g = voxelColor.r;
                voxelColor.b = voxelColor.r;
                voxelColor.a = val > 0 ? 1.0 : 0.0;
                // this makes intensity 0 completely transparent and the rest opaque
                // Alternatively, apply the transfer function to obtain a color
                //voxelColor = tFunc.getColor(val);

                // BufferedImage expects a pixel color packed as ARGB in an int
                int c_alpha = voxelColor.a <= 1.0 ? (int) Math.floor(voxelColor.a * 255) : 255;
                int c_red = voxelColor.r <= 1.0 ? (int) Math.floor(voxelColor.r * 255) : 255;
                int c_green = voxelColor.g <= 1.0 ? (int) Math.floor(voxelColor.g * 255) : 255;
                int c_blue = voxelColor.b <= 1.0 ? (int) Math.floor(voxelColor.b * 255) : 255;
                int pixelColor = (c_alpha << 24) | (c_red << 16) | (c_green << 8) | c_blue;
                image.setRGB(i, j, pixelColor);
            }
        }
    }

    private void drawBoundingBox(GL2 gl) {
        gl.glPushAttrib(GL2.GL_CURRENT_BIT);
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glColor4d(1.0, 1.0, 1.0, 1.0);
        gl.glLineWidth(1.5f);
        gl.glEnable(GL.GL_LINE_SMOOTH);
        gl.glHint(GL.GL_LINE_SMOOTH_HINT, GL.GL_NICEST);
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glBegin(GL.GL_LINE_LOOP);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glVertex3d(-volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, volume.getDimZ() / 2.0);
        gl.glVertex3d(volume.getDimX() / 2.0, -volume.getDimY() / 2.0, -volume.getDimZ() / 2.0);
        gl.glEnd();

        gl.glDisable(GL.GL_LINE_SMOOTH);
        gl.glDisable(GL.GL_BLEND);
        gl.glEnable(GL2.GL_LIGHTING);
        gl.glPopAttrib();

    }

    @Override
    public void visualize(GL2 gl) {

        if (volume == null) {
            return;
        }

        drawBoundingBox(gl);

        gl.glGetDoublev(GL2.GL_MODELVIEW_MATRIX, viewMatrix, 0);

        long startTime = System.currentTimeMillis();

        if (RaycastRenderer.isPhong) {
            if (RaycastRenderer.typeRendering == RaycastRenderer.SLICER) {
                slicerPhong(viewMatrix);
            } else if (RaycastRenderer.typeRendering == RaycastRenderer.MIP) {
                MIPPhong(viewMatrix);
            } else if (RaycastRenderer.typeRendering == RaycastRenderer.COMPOSITING) {
                compositingPhong(viewMatrix);
            } else if (RaycastRenderer.typeRendering == RaycastRenderer.TRANSFER2D) {
                transferFunction2DPhong(viewMatrix);
            }
        } else {
            if (RaycastRenderer.typeRendering == RaycastRenderer.SLICER) {
                slicer(viewMatrix);
            } else if (RaycastRenderer.typeRendering == RaycastRenderer.MIP) {
                MIP(viewMatrix);
            } else if (RaycastRenderer.typeRendering == RaycastRenderer.COMPOSITING) {
                compositing(viewMatrix);
            } else if (RaycastRenderer.typeRendering == RaycastRenderer.TRANSFER2D) {
                transferFunction2D(viewMatrix);
            }
        }
        long endTime = System.currentTimeMillis();
        double runningTime = (endTime - startTime);
        panel.setSpeedLabel(Double.toString(runningTime));

        Texture texture = AWTTextureIO.newTexture(gl.getGLProfile(), image, false);

        gl.glPushAttrib(GL2.GL_LIGHTING_BIT);
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

        // draw rendered image as a billboard texture
        texture.enable(gl);
        texture.bind(gl);
        double halfWidth = image.getWidth() / 2.0;
        gl.glPushMatrix();
        gl.glLoadIdentity();
        gl.glBegin(GL2.GL_QUADS);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glTexCoord2d(0.0, 0.0);
        gl.glVertex3d(-halfWidth, -halfWidth, 0.0);
        gl.glTexCoord2d(0.0, 1.0);
        gl.glVertex3d(-halfWidth, halfWidth, 0.0);
        gl.glTexCoord2d(1.0, 1.0);
        gl.glVertex3d(halfWidth, halfWidth, 0.0);
        gl.glTexCoord2d(1.0, 0.0);
        gl.glVertex3d(halfWidth, -halfWidth, 0.0);
        gl.glEnd();
        texture.disable(gl);
        texture.destroy(gl);
        gl.glPopMatrix();

        gl.glPopAttrib();

        if (gl.glGetError() > 0) {
            System.out.println("some OpenGL error: " + gl.glGetError());
        }

    }
    private BufferedImage image;
    private double[] viewMatrix = new double[4 * 4];

    @Override
    public void changed() {
        for (int i = 0; i < listeners.size(); i++) {
            listeners.get(i).changed();
        }
    }
}
