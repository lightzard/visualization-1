/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package volume;

/**
 *
 * @author michel
 */
public class GradientVolume {

    public GradientVolume(Volume vol) {
        volume = vol;
        dimX = vol.getDimX();
        dimY = vol.getDimY();
        dimZ = vol.getDimZ();
        data = new VoxelGradient[dimX * dimY * dimZ];
        compute();
        maxmag = -1.0;
    }
    
    public VoxelGradient getGradient(double x, double y, double z) {
        int xx= (int) Math.round(x);
        int yy= (int) Math.round(y);
        int zz= (int) Math.round(z);
        if(xx<0 || xx>=volume.getDimX() || yy<0 || yy>=volume.getDimY()||zz<0||zz>=volume.getDimZ())return new VoxelGradient();
        return data[xx + dimX * (yy + dimY * zz)];
    }
    
    public VoxelGradient getGradient(int x, int y, int z) {
        return data[x + dimX * (y + dimY * z)];
    }

    
    public void setGradient(int x, int y, int z, VoxelGradient value) {
        data[x + dimX * (y + dimY * z)] = value;
    }

    public void setVoxel(int i, VoxelGradient value) {
        data[i] = value;
    }

    public VoxelGradient getVoxel(int i) {
        return data[i];
    }

    public int getDimX() {
        return dimX;
    }

    public int getDimY() {
        return dimY;
    }

    public int getDimZ() {
        return dimZ;
    }

    private void compute() {
        System.out.println("Start computing gradient vector");
        // this just initializes all gradients to the vector (0,0,0)
        for (int i=0; i<data.length; i++) {
            int dimXx=this.getDimX();
            int dimYy=this.getDimY();
            //data[i] = one;
            int z=i/(dimXx*dimYy);
            int y=(i%(dimXx*dimYy))/dimXx;
            int x=((i%(dimXx*dimYy))%dimXx);
            
            data[i]=new VoxelGradient((float) (0.5*(getVoxelByInterpolation(x+1,y,z)-getVoxelByInterpolation(x-1,y,z))),(float) (0.5*(getVoxelByInterpolation(x,y+1,z)-getVoxelByInterpolation(x,y-1,z))),(float) (0.5*(getVoxelByInterpolation(x,y,z+1)-getVoxelByInterpolation(x,y,z-1))));
            //System.out.println(data[i].mag);

        }
        System.out.println("Done computing gradient vector");        
    }
    
    public double getMaxGradientMagnitude() {
        if (maxmag >= 0) {
            return maxmag;
        } else {
            double magnitude = data[0].mag;
            for (int i=0; i<data.length; i++) {
                magnitude = data[i].mag > magnitude ? data[i].mag : magnitude;
            }   
            maxmag = magnitude;
            return magnitude;
        }
    }
    
    private int dimX, dimY, dimZ;
    private VoxelGradient zero = new VoxelGradient();
    private VoxelGradient one = new VoxelGradient(1,1,1); 
    VoxelGradient[] data;
    Volume volume;
    double maxmag;
    
    public short getSafeVoxel(int xx,int yy, int zz){
        if(xx<=0 || yy<=0 || zz<=0||xx>=volume.getDimX() || yy>=volume.getDimY() || zz>=volume.getDimZ())return 0;
        return volume.getVoxel(xx,yy,zz);
    }

    public short getVoxelByInterpolation(float xx,float yy,float zz) {
        float[] coord={xx,yy,zz};
        if (coord[0] < 0 || coord[0] > volume.getDimX() || coord[1] < 0 || coord[1] > volume.getDimY()
                || coord[2] < 0 || coord[2] > volume.getDimZ()) {
            return 0;
        }

        int x = (int) Math.floor(coord[0]);
        int y = (int) Math.floor(coord[1]);
        int z = (int) Math.floor(coord[2]);
        
        int[] x0={x,y,z};
        int[] x1={x+1,y,z};
        int[] x2={x,y+1,z};
        int[] x3={x+1,y+1,z};
        int[] x4={x,y,z+1};
        int[] x5={x+1,y,z+1};
        int[] x6={x,y+1,z+1};
        int[] x7={x+1,y+1,z+1};
        
        double alfa=coord[0]-x;
        double beta=coord[1]-y;
        double gamma=coord[2]-z;
        
        return (short)(
                 (1-alfa)*(1-beta)*(1-gamma)*getSafeVoxel(x0[0],x0[1],x0[2])
                +alfa*(1-beta)*(1-gamma)*getSafeVoxel(x1[0],x1[1],x1[2])
                +(1-alfa)*beta*(1-gamma)*getSafeVoxel(x2[0],x2[1],x2[2])
                +alfa*beta*(1-gamma)*getSafeVoxel(x3[0],x3[1],x3[2])
                +(1-alfa)*(1-beta)*gamma*getSafeVoxel(x4[0],x4[1],x4[2])
                +alfa*(1-beta)*gamma*getSafeVoxel(x5[0],x5[1],x5[2])
                +(1-alfa)*beta*gamma*getSafeVoxel(x6[0],x6[1],x6[2])
                +alfa*beta*gamma*getSafeVoxel(x7[0],x7[1],x7[2])
                );
        //return volume.getVoxel(x, y, z);
    }


}
